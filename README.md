# CompNet_Ass2

Computer Network Assignment 2: Building the network system for the bank

## How to run
Open the `.pkt` file with cisco packet tracer >= 8.0.1

Wait for a few seconds for all the network devices to be boosted

## Brief descrition of the project
In this project, we construct the network for the bank system which has 3 sites 1 headquarter and 2 branches.

For each site, we build up the LAN while connecting between sites we use WAN.
